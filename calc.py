#!/usr/bin/python3

from sys import argv

def sumar(m,n):
    return m+n

def restar(m,n):
    return m-n

if __name__ == "__main__":
    resultado1 = sumar(1, 2)
    print(f"Resultado = {resultado1}")

    resultado2 = sumar(3, 4)
    print(f"Resultado = {resultado2}")

    resultado3 = restar(5, 6)
    print(f"Resultado = {resultado3}")

    resultado4 = restar(7, 8)
    print(f"Resultado = {resultado4}")